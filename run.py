import sys
from crawler.main import Crawler

if __name__ == "__main__":
    c = Crawler("https://www.w3.org")
    if len(sys.argv) == 2:
        if sys.argv[1] == "debug":
            c = Crawler("https://www.w3.org", debug=True)
    c.start()
    c.join()

from html.parser import HTMLParser
from urllib import parse
from urllib.request import urlopen

from .main import Logger

class LinkParser(HTMLParser):
    def __init__(self, url):
        HTMLParser.__init__(self)
        self.url = url

    def getLinks(self):
        self.links = []

        Logger.log("Loading %s" % self.url)
        response = urlopen(self.url)
        if "text/html" in response.getheader("Content-Type"):
            htmlBytes = response.read()
            htmlString = htmlBytes.decode("utf-8")
            self.feed(htmlString)
            return self.links
        else:
            return []

    def handle_starttag(self, tag, attrs):
        if tag == "a":
            counter = 0
            for (key, value) in attrs:
                if key == "href":
                    self.links.append(value)
                    counter += 1
            Logger.log(("Found %d links" % counter), debug=True)

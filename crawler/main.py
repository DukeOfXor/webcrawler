from threading import Thread
import time


is_debug = False

class Crawler(Thread):
    def __init__(self, root_url, debug=False):
        Thread.__init__(self)
        self.root_url = root_url
        global is_debug
        is_debug = debug
        Logger.log("Crawler initialized with url %s" % self.root_url)

    def run(self):
        Logger.log("Crawler started")
        from .parser import LinkParser
        parser = LinkParser(self.root_url)
        links = parser.getLinks()
        Logger.log("Crawler finished")

class Logger:
    @classmethod
    def log(log_message, debug=False):
        cur_time = time.strftime('%X')

        if debug:
            if is_debug:
                print("[%s][DEBUG] %s" % (cur_time, log_message))
                return
            else:
                return

        print("[%s] %s" % (cur_time, log_message))

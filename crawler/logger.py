import time

import crawler

def log(log_message, debug=False):
    cur_time = time.strftime('%X')

    if debug:
        if crawler.is_debug:
            print("[%s][DEBUG] %s" % (cur_time, log_message))
            return
        else:
            return

    print("[%s] %s" % (cur_time, log_message))
